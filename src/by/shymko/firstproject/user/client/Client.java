package by.shymko.firstproject.user.client;

import by.shymko.firstproject.exceptions.LogicalException;
import by.shymko.firstproject.list.GoodsList;
import by.shymko.firstproject.user.User;
import org.apache.log4j.Logger;


/**
 * Created by Andrey on 17.02.2015.
 */
public class Client extends User {
    private static final Logger LOG = Logger.getLogger(Client.class);
    private GoodsList order;
    private int budget;
    private boolean activity;

    public Client() {
    }

    public Client(int id, String name) {
        super(id, name);
    }

    public Client(int id, String name, int budget, boolean activity) {
        super(id, name);
        this.budget = budget;
        this.activity = activity;
    }

    public GoodsList getOrder() {
        return order;
    }

    public void setOrder(GoodsList order) throws LogicalException {
        if (order == null) {
            LOG.info("orderList is null");
            throw new LogicalException();
        }
        this.order = order;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) throws LogicalException {
        if (budget < 0) {
            LOG.info("invalid budget");
            throw new LogicalException();
        }
        this.budget = budget;
    }

    public boolean isActivity() {
        return activity;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "Client{" + "id=" + super.getId() +
                ", name='" + super.getName() +
                "order=" + order +
                ", budget=" + budget +
                ", activity=" + activity +
                '}';
    }
}
