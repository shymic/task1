package by.shymko.firstproject.user.administrator;

import by.shymko.firstproject.user.client.Client;
import by.shymko.firstproject.exceptions.LogicalException;
import by.shymko.firstproject.goods.Good;
import by.shymko.firstproject.list.ClientList;
import by.shymko.firstproject.list.GoodsList;
import by.shymko.firstproject.user.User;
import org.apache.log4j.Logger;


/**
 * Created by Andrey on 17.02.2015.
 */
public class Administrator extends User {
    private static final Logger LOG = Logger.getLogger(Administrator.class);
    private String contact;
    private GoodsList priseList;
    private ClientList clientList;

    public Administrator() {
    }

    public Administrator(int id, String name) {
        super(id, name);
    }

    public Administrator(int id, String name, String contact,
                         ClientList clientList, GoodsList priseList) {
        super(id, name);
        this.contact = contact;
        this.priseList = priseList;
        this.clientList = clientList;
    }

    public Administrator(int id, String name, String contact) {
        super(id, name);
        this.contact = contact;
        priseList = new GoodsList();
        clientList = new ClientList();
    }


    public String getContact() {
        return contact;
    }

    public void setContact(String contact) throws LogicalException {
        if (contact.length() == 0) {
            LOG.error("invalid contact");
            throw new LogicalException();
        }
        this.contact = contact;
    }

    public GoodsList getPriseList() {
        return priseList;
    }

    public void setPriseList(GoodsList priseList) throws LogicalException {
        if (priseList == null) {
            LOG.error("priseList is null");
            throw new LogicalException();
        }
        this.priseList = priseList;
    }

    public ClientList getClientList() {
        return clientList;
    }

    public void setClientList(ClientList clientList) throws LogicalException {
        if (clientList == null) {
            LOG.error("clientList is null");
            throw new LogicalException();
        }
        this.clientList = clientList;
    }

    public boolean addClient(Client client) {
        return clientList.add(client);
    }

    public boolean removeGood(int id) {
        return getPriseList().removeById(id);
    }

    public boolean addGood(Good good) {
        return priseList.add(good);
    }

    public boolean blockClient(int id) {
        if (clientList.findById(id) != null) {
            clientList.findById(id).setActivity(false);
            LOG.info(" client was blocked");
            return true;
        } else {
            LOG.info("id not found");
            return false;
        }
    }

    public boolean unblockClient(int id) {
        if (clientList.findById(id) != null) {
            clientList.findById(id).setActivity(true);
            LOG.info(" client was unblocked");
            return true;
        } else {
            LOG.info("id not found");
            return false;
        }
    }

    @Override
    public String toString() {
        return "Administrator{" + "id=" + super.getId() + '\n' +
                ", name='" + super.getName() + '\n' +
                "contact='" + contact + '\n' +
                ", priseList=" + priseList + '\n' +
                ", clientList=" + clientList + '\n' +
                '}';
    }
}
