package by.shymko.firstproject.user;

import by.shymko.firstproject.exceptions.LogicalException;

/**
 * Created by Andrey on 17.02.2015.
 */
public abstract class User {
    private int id;
    private String name;

    protected User() {
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws LogicalException {
        if (id < 0) {
            throw new LogicalException();
        }
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws LogicalException {
        if (name.length() == 0) {
            throw new LogicalException();
        }
        this.name = name;
    }


}
