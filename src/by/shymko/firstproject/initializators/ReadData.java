package by.shymko.firstproject.initializators;

import by.shymko.firstproject.user.administrator.Administrator;
import by.shymko.firstproject.user.client.Client;
import by.shymko.firstproject.exceptions.TecnicalException;
import by.shymko.firstproject.goods.Good;
import by.shymko.firstproject.list.ClientList;
import by.shymko.firstproject.list.GoodsList;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by Andrey on 17.02.2015.
 */
public class ReadData {
    private static final Logger LOG = Logger.getLogger(ReadData.class);

    public static Administrator readData(String fileName) throws TecnicalException {
        try (FileReader fileReader = new FileReader(fileName)) {
            LOG.debug("file opened");
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(fileReader);
            JSONObject jsonObject = (JSONObject) obj;

            Long id = (Long) jsonObject.get("id");
            String name = (String) jsonObject.get("name");
            String contact = (String) jsonObject.get("contact");

            return new Administrator(id.intValue(), name, contact, readClientList(jsonObject), readGoodList(jsonObject));
        } catch (FileNotFoundException e) {
            LOG.error(" FileNotFoundException", e);
            throw new TecnicalException(e);
        } catch (ParseException e) {
            LOG.error("ParseException", e);
            throw new TecnicalException(e);
        } catch (IOException e) {
            LOG.error("IOException", e);
            throw new TecnicalException(e);
        } finally {
            LOG.info("reading finished");
        }
    }

    private static GoodsList readGoodList(JSONObject jsonObject) {
        GoodsList priseList = new GoodsList();
        JSONArray goods = (JSONArray) jsonObject.get("goods");

        Iterator<JSONObject> iterator = goods.iterator();

        while (iterator.hasNext()) {
            JSONObject jsObject = iterator.next();
            Long id = (Long) jsObject.get("id");
            String name = (String) jsObject.get("name");
            Long prise = (Long) jsObject.get("prise");
            Long sale = (Long) jsObject.get("sale");
            String info = (String) jsObject.get("info");
            Long quantity = (Long) jsObject.get("quantity");
            priseList.add(new Good(id.intValue(), name, prise.intValue(), sale.intValue(), info, quantity.intValue()));
        }
        return priseList;
    }

    private static ClientList readClientList(JSONObject jsonObject) {
        ClientList clientsList = new ClientList();
        JSONArray clients = (JSONArray) jsonObject.get("clients");

        Iterator<JSONObject> iterator = clients.iterator();

        while (iterator.hasNext()) {
            JSONObject jsObject = iterator.next();
            Long id = (Long) jsObject.get("id");
            String name = (String) jsObject.get("name");
            Long budget = (Long) jsObject.get("budget");
            boolean status = (Boolean) jsObject.get("status");
            clientsList.add(new Client(id.intValue(), name, budget.intValue(), status));
        }
        return clientsList;
    }

}
