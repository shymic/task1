package by.shymko.firstproject.comparator.goodcomparator;

import by.shymko.firstproject.goods.Good;

import java.util.Comparator;

/**
 * Created by Andrey on 22.02.2015.
 */
public class PriseComparator implements Comparator<Good> {
    @Override
    public int compare(Good o1, Good o2) {
        return Integer.compare(o1.getPrise(), o2.getPrise());
    }
}
