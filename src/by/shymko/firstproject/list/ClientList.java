package by.shymko.firstproject.list;


import by.shymko.firstproject.user.client.Client;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

/**
 * Created by Andrey on 17.02.2015.
 */
public class ClientList extends ArrayList<Client> {
    public ClientList(int initialCapacity) {
        super(initialCapacity);
    }

    public ClientList() {
    }

    public Client findById(int id) {
        for (int i = 0; i < this.size(); ++i) {
            if (this.get(i).getId() == id) {
                return this.get(i);
            }
        }
        return null;
    }


}
