package by.shymko.firstproject.internetshop;

import by.shymko.firstproject.user.administrator.Administrator;
import by.shymko.firstproject.exceptions.TecnicalException;
import by.shymko.firstproject.initializators.ReadData;
import by.shymko.firstproject.user.User;

import java.util.ArrayList;

/**
 * Created by Andrey on 17.02.2015.
 */
public class Shop {
    private static final String DATA = "src\\by\\shymko\\firstproject\\resourses\\data.json";
    private ArrayList<User> staff;
    private Administrator admin;

    public Shop(Administrator admin) {
        this.admin = admin;
        initStaff();
    }

    public Shop() throws TecnicalException {
        admin = ReadData.readData(DATA);
        initStaff();
    }

    public Administrator getAdmin() {
        return admin;
    }

    public void initStaff() {
        staff = new ArrayList<User>();
        staff.add(admin);
        staff.addAll(admin.getClientList());
    }

    @Override
    public String toString() {
        return "Shop{" +
                "staff=" + staff +
                ", admin=" + admin +
                '}';
    }
}