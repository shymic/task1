package by.shymko.firstproject.main;

import by.shymko.firstproject.comparator.usercomparator.IdComparator;
import by.shymko.firstproject.comparator.usercomparator.NameComparator;
import by.shymko.firstproject.exceptions.TecnicalException;
import by.shymko.firstproject.internetshop.Shop;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Andrey on 17.02.2015.
 */
public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class);

    static {
        new DOMConfigurator().doConfigure("src\\by\\shymko\\firstproject\\resourses\\log4j.xml", LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        try {
            LOG.info("Start");
            Shop shop = new Shop();
            System.out.println(shop.toString());
            shop.getAdmin().getClientList().sort(new IdComparator());
            System.out.println(shop.toString());
            shop.getAdmin().getClientList().sort(new NameComparator());
            System.out.println(shop.toString());
            LOG.debug("finish");
        } catch (TecnicalException e) {
            LOG.info("exception" + e);
        }
    }
}
